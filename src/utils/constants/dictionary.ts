export const commonText = {
  number: '#',
  page: 'page',
  true: 'true',
  ampersand: '&',
  search: 'Search',
  showByLimit: 'Show by:',

  headerMenuItems: {
    pokedex: 'Pokedex',
  },
};

export const pokedexText = {
  noData: 'No Data',
  surpriseMe: 'Surprise me!',
  regions: 'Regions',
  region: 'region',
  types: 'Types',
  type: 'type',
  random: 'random',
  favorites: 'favorites',
  filters: 'Filters',
  reset: 'Reset',
};

export const actionsText = {
  pokedex: {
    name: 'pokedex',
    limit: 'limit',
    offset: 'offset',
    fetchAllPokemons: 'fetchAllPokemons',
    fetchPokemonInfo: 'fetchPokemonInfo',
    fetchPokemonType: 'fetchPokemonType',
    fetchPokemonRegion: 'fetchPokemonRegion',
    fetchFilteredPokemons: 'fetchFilteredPokemons',
    fetchRandomPokemonInfo: 'fetchRandomPokemonInfo',
  },
};

import { IHeaderMenuItem } from 'models/common';
import { commonText } from 'utils/constants/dictionary';
import { routes } from 'utils/constants/routes';

export const headerMenuItems: IHeaderMenuItem[] = [
  {
    path: routes.pokedex,
    name: commonText.headerMenuItems.pokedex,
  },
];

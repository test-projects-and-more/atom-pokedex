import { variables } from 'assets/styles/variables';
import { IPokedexFilters } from 'models/pokedex';

export const startPage = 1;

export const padString = '0';
export const padTargetLength = 3;

export const limits = ['24', '48', '66'];

export const pokemonFirstPartIdMin = 1;
export const pokemonFirstPartIdMax = 905;

export const pokemonSecondPartIdMin = 10001;
export const pokemonSecondPartIdMax = 10249;

export const randomPartDistribution = 0.9;

export const regionFilterValues = Object.entries(
  variables.color.pokedex.regions
);
export const typeFilterValues = Object.entries(
  variables.color.pokedex.pokemonTypes
);

export const defaultPokedexFilters: IPokedexFilters = {
  typeFilter: '',
  regionFilter: '',
};

export const surpriseButtonDefaultColor = 14;
export const pokemonTypesColors = Object.values(
  variables.color.pokedex.pokemonTypes
);

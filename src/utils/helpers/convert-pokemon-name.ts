interface IConvertedPokemonName {
  name: string;
  version: string;
}

export const convertPokemonName = (name: string): IConvertedPokemonName => {
  const nameParts = name.split('-');

  return nameParts.reduce(
    (acc, namePart, index) => {
      if (index === 0) {
        acc = { name: namePart, version: acc.version };
        return acc;
      }

      if (namePart.length < 3) {
        acc = { name: `${acc.name}-${namePart}`, version: acc.version };
        return acc;
      }

      acc = {
        name: acc.name,
        version: acc.version.length ? `${acc.version}-${namePart}` : namePart,
      };
      return acc;
    },
    { name: '', version: '' }
  );
};

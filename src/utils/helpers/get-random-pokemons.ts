import {
  pokemonFirstPartIdMax,
  pokemonFirstPartIdMin,
  pokemonSecondPartIdMax,
  pokemonSecondPartIdMin,
  randomPartDistribution,
} from 'utils/constants/pokedex';

interface IRandomIds {
  min: number;
  max: number;
}

const getRandomPokemonId = (): IRandomIds => {
  if (Math.random() < randomPartDistribution) {
    return { min: pokemonFirstPartIdMin, max: pokemonFirstPartIdMax };
  }

  return { min: pokemonSecondPartIdMin, max: pokemonSecondPartIdMax };
};

export const getRandomPokemons = (limit: number) => {
  const randomPokemonIds = new Set<number>();

  while (randomPokemonIds.size < limit) {
    const { min, max } = getRandomPokemonId();

    const randomId = min + Math.random() * (max + 1 - min);
    randomPokemonIds.add(Math.floor(randomId));
  }

  return randomPokemonIds;
};

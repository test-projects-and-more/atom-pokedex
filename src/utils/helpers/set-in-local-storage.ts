export const setInLocalStorage = (
  key: string,
  value: string | object
): void => {
  if (typeof value === 'object') {
    value = JSON.stringify(value);
  }

  localStorage.setItem(key, value);
};

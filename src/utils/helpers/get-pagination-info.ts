import { limits, startPage } from 'utils/constants/pokedex';

interface IGetPaginationOffset {
  limit?: number;
  pageNumber?: number;
}

interface IGetPaginationPagesCount {
  limit?: number;
  count: number;
}

export const getPaginationOffset = ({
  pageNumber = startPage,
  limit = Number(limits[0]),
}: IGetPaginationOffset): number => {
  return (pageNumber - 1) * limit;
};

export const getPaginationPagesCount = ({
  count,
  limit = Number(limits[0]),
}: IGetPaginationPagesCount): number => {
  return Math.ceil(count / limit);
};

import { favoritesLocalStorage } from 'utils/constants/local-storage';
import { getFromLocalStorage } from 'utils/helpers/get-from-local-storage';
import { setInLocalStorage } from 'utils/helpers/set-in-local-storage';

export const addPokemonToFavorites = (id: number, isFavorite: boolean) => {
  if (!getFromLocalStorage(favoritesLocalStorage)) {
    setInLocalStorage(favoritesLocalStorage, []);
  }

  const favorites = getFromLocalStorage(favoritesLocalStorage);

  if (!isFavorite) {
    favorites.push(id);
    setInLocalStorage(favoritesLocalStorage, favorites);
    return;
  }

  setInLocalStorage(
    favoritesLocalStorage,
    favorites.filter((likedId: number) => likedId !== id)
  );
};

import { arrayBracket, objectBracket } from 'utils/constants/local-storage';

export const getFromLocalStorage = (key: string) => {
  const value = localStorage.getItem(key);

  if (value) {
    if (value[0] === objectBracket || value[0] === arrayBracket) {
      return JSON.parse(value);
    }

    return value;
  }

  return '';
};

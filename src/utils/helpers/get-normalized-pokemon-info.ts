import { IPokemonSprites, IPokemonType } from 'models/pokedex';
import { IPokemonRequestSprites } from 'models/pokemon-response';
import { favoritesLocalStorage } from 'utils/constants/local-storage';
import { getFromLocalStorage } from 'utils/helpers/get-from-local-storage';

interface IGetNormalizedPokemonInfo {
  types: IPokemonType[];
  sprites: IPokemonRequestSprites;
}

interface INormalizedPokemonInfo {
  pokemonImages: IPokemonSprites;
  normalizedTypes: string[];
  favorites: number[];
}

export const getNormalizedPokemonInfo = ({
  types,
  sprites,
}: IGetNormalizedPokemonInfo): INormalizedPokemonInfo => {
  const pokemonImages: IPokemonSprites = {
    spriteFrontDefault: sprites.front_default,
    spriteBackDefault: sprites.back_default,
    spriteFrontDefaultFemale: sprites.front_female,
    spriteBackDefaultFemale: sprites.back_female,
    spriteFrontShiny: sprites.front_shiny,
    spriteBackShiny: sprites.back_shiny,
    spriteFrontShinyFemale: sprites.front_shiny_female,
    spriteBackShinyFemale: sprites.back_shiny_female,
    animatedFrontDefault:
      sprites.versions['generation-v']['black-white'].animated.front_default,
    animatedFrontShiny:
      sprites.versions['generation-v']['black-white'].animated.front_shiny,
    officialFrontDefault: sprites.other['official-artwork'].front_default,
    homeFrontDefault: sprites.other.home.front_default,
    homeFrontDefaultFemale: sprites.other.home.front_female,
    homeFrontShiny: sprites.other.home.front_shiny,
    homeFrontShinyFemale: sprites.other.home.front_shiny_female,
    icon: sprites.versions['generation-viii'].icons.front_default,
  };

  const normalizedTypes = types.reduce((acc: string[], type: IPokemonType) => {
    acc.push(type.type.name);
    return acc;
  }, []);

  const favorites = getFromLocalStorage(favoritesLocalStorage);

  return {
    normalizedTypes,
    pokemonImages,
    favorites,
  };
};

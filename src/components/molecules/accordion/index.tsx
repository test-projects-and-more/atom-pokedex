import { FC, ReactElement } from 'react';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { AccordionDetails, AccordionSummary } from '@mui/material';
import { StyledAccordion } from 'components/molecules/accordion/style';

interface IAccordionProps {
  title: string;
  content: ReactElement;
}

const Accordion: FC<IAccordionProps> = ({ title, content }) => {
  return (
    <StyledAccordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        {title}
      </AccordionSummary>

      <AccordionDetails>{content}</AccordionDetails>
    </StyledAccordion>
  );
};

export default Accordion;

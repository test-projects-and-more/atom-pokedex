import { Accordion } from '@mui/material';
import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const StyledAccordion = styled(Accordion)`
  &[class$='MuiAccordion-root'] {
    background-color: ${variables.color.sideMenu.accordion.backgroung};
    border-bottom: 1px solid ${variables.color.sideMenu.accordion.border};

    &.Mui-expanded {
      margin: 0;
    }

    .Mui-expanded {
      margin: 0;

      &.MuiButtonBase-root {
        min-height: 3rem;
        border-bottom: 1px solid ${variables.color.sideMenu.accordion.border};
      }
    }

    &:last-child {
      border-bottom: 0;
    }

    &:before {
      display: none;
    }

    .MuiCollapse-root {
      background-color: ${variables.color.sideMenu.accordion
        .collapseBackground};
    }

    .MuiAccordionDetails-root {
      padding: 1.25rem 1rem;
    }
  }
`;

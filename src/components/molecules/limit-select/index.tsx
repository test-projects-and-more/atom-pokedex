import { FC } from 'react';

import { Select, MenuItem, SelectChangeEvent } from '@mui/material';
import { Limit } from 'components/molecules/limit-select/style';
import { commonText } from 'utils/constants/dictionary';
import { limits } from 'utils/constants/pokedex';

interface ILimitSelectProps {
  value: string;
  defaultValue: string;
  handleChange: (event: SelectChangeEvent) => void;
}

const LimitSelect: FC<ILimitSelectProps> = ({
  value,
  defaultValue,
  handleChange,
}) => {
  return (
    <Limit>
      <span className="limit-text">{commonText.showByLimit}</span>

      <Select value={value} onChange={handleChange} defaultValue={defaultValue}>
        {limits.map((value) => {
          return (
            <MenuItem key={value} value={value}>
              {value}
            </MenuItem>
          );
        })}
      </Select>
    </Limit>
  );
};

export default LimitSelect;

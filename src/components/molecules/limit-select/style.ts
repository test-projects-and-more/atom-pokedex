import styled from 'styled-components';

export const Limit = styled.div`
  display: flex;
  align-items: center;

  .limit-text {
    margin-right: 1rem;
  }

  .MuiSelect-select {
    padding: 0.313rem 1.875rem 0.313rem 0.625rem;
  }
`;

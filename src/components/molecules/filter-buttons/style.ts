import Button from 'components/atoms/button';
import styled from 'styled-components';

export const StyledButton = styled(Button)`
  &[class$='MuiButton-root'] {
    margin-right: 1rem;
    margin-bottom: 1rem;

    &.inactive {
      opacity: 0.4;
    }
  }
`;

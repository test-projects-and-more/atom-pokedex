import { FC } from 'react';

import { StyledButton } from 'components/molecules/filter-buttons/style';

interface IFilterButtonsProps {
  filter: string;
  filterValues: string[][];
  handleButtonClick: (value: string) => void;
}

const FilterButtons: FC<IFilterButtonsProps> = ({
  filter,
  filterValues,
  handleButtonClick,
}) => {
  const handleGetActiveFilter = (filter: string, value: string) => {
    return filter && value !== filter;
  };

  return (
    <>
      {filterValues.map((filterValue) => (
        <StyledButton
          key={filterValue[0]}
          buttonColor={filterValue[1]}
          onClick={() => handleButtonClick(filterValue[0])}
          className={`${
            handleGetActiveFilter(filter, filterValue[0]) && 'inactive'
          }`}
        >
          {filterValue[0]}
        </StyledButton>
      ))}
    </>
  );
};

export default FilterButtons;

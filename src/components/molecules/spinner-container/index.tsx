import { FC } from 'react';

import Spinner from 'components/atoms/spinner';
import { StyledContainer } from 'components/molecules/spinner-container/style';

const SpinnerContainer: FC = () => {
  return (
    <StyledContainer>
      <Spinner />
    </StyledContainer>
  );
};

export default SpinnerContainer;

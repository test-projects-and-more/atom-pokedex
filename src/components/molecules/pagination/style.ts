import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const PaginationContainer = styled.div`
  position: sticky;
  bottom: 0;
  z-index: 10;
  display: flex;
  padding: 1.25rem 2.25rem;
  justify-content: space-between;
  background: ${variables.color.common.paginationBackground};
`;

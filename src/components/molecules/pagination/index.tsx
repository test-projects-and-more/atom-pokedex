import { FC, useEffect } from 'react';

import {
  PaginationItem,
  SelectChangeEvent,
  Pagination as StyledPagination,
} from '@mui/material';
import LimitSelect from 'components/molecules/limit-select';
import { PaginationContainer } from 'components/molecules/pagination/style';
import { Link } from 'react-router-dom';
import { commonText } from 'utils/constants/dictionary';

interface IPaginationProps {
  path: string;
  limit: string;
  params?: string;
  pageNumber: number;
  pagesCount: number;
  defaultLimitValue: string;
  handlePaginationChange: () => void;
  handleLimitChange: (event: SelectChangeEvent) => void;
}

const Pagination: FC<IPaginationProps> = ({
  path,
  limit,
  params,
  pageNumber,
  pagesCount,
  defaultLimitValue,
  handleLimitChange,
  handlePaginationChange,
}) => {
  useEffect(() => {
    handlePaginationChange();
  }, [pageNumber]);

  return (
    <PaginationContainer>
      <StyledPagination
        shape="rounded"
        page={pageNumber}
        count={pagesCount}
        renderItem={(paginationItem) => (
          <PaginationItem
            component={Link}
            to={`${path}?${commonText.page}=${paginationItem.page}${params}`}
            {...paginationItem}
          />
        )}
      />

      <LimitSelect
        value={limit}
        handleChange={handleLimitChange}
        defaultValue={defaultLimitValue}
      />
    </PaginationContainer>
  );
};

export default Pagination;

import { FC } from 'react';

import FavoriteIcon from '@mui/icons-material/Favorite';
import { SelectChangeEvent } from '@mui/material';
import { variables } from 'assets/styles/variables';
import Button from 'components/atoms/button';
import LimitSelect from 'components/molecules/limit-select';
import { StyledPanel } from 'components/molecules/random-panel/style';
import SurpriseButton from 'components/molecules/surprise-button';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router';
import { changePokedexLimit } from 'redux/pokedex';
import { getPokedexLimit } from 'redux/pokedex/selectors';
import { AppDispatch } from 'redux/store';
import { commonText, pokedexText } from 'utils/constants/dictionary';
import { startPage } from 'utils/constants/pokedex';
import { routes } from 'utils/constants/routes';

const RandomPanel: FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const limit = useSelector(getPokedexLimit);

  const navigate = useNavigate();
  const location = useLocation();

  const isRandomPokedex = location.search.includes(pokedexText.random);

  const handleLimitChange = (event: SelectChangeEvent) => {
    const limit = event.target.value;

    dispatch(changePokedexLimit(limit));
  };

  const handleGetFavorites = () => {
    navigate({
      pathname: routes.pokedex,
      search: `?${commonText.page}=${startPage}&${pokedexText.favorites}=${commonText.true}`,
    });
  };

  return (
    <StyledPanel>
      <Button
        startIcon={<FavoriteIcon />}
        onClick={handleGetFavorites}
        buttonColor={variables.color.sideMenu.button.favorites}
      >
        {pokedexText.favorites}
      </Button>

      <SurpriseButton limit={limit} />

      {isRandomPokedex && (
        <div className="limit">
          <LimitSelect
            value={limit}
            defaultValue={limit}
            handleChange={handleLimitChange}
          />
        </div>
      )}
    </StyledPanel>
  );
};

export default RandomPanel;

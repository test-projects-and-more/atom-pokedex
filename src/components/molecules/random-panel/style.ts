import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const StyledPanel = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 2.25rem 0;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  border-top: 2px solid ${variables.color.sideMenu.border};
  border-bottom: 2px solid ${variables.color.sideMenu.border};

  button:first-child {
    margin-bottom: 1.25rem;
  }

  .limit {
    margin-top: 1.25rem;
  }
`;

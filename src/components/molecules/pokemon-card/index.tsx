import { FC, useEffect, useState } from 'react';

import { CardActionArea } from '@mui/material';
import QuestionIcon from 'assets/icons/question-icon';
import LikeButton from 'components/atoms/like-button';
import {
  StyledContent,
  ImageContainer,
  StyledPokemonCard,
} from 'components/molecules/pokemon-card/style';
import PokemonTypes from 'components/molecules/pokemon-types';
import { IPokedexPokemon } from 'models/pokedex';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router';
import { fetchPokemonInfo, likePokemon } from 'redux/pokedex';
import { AppDispatch } from 'redux/store';
import { commonText, pokedexText } from 'utils/constants/dictionary';
import { padString, padTargetLength } from 'utils/constants/pokedex';
import { addPokemonToFavorites } from 'utils/helpers/add-pokemon-to-favorites';
import { convertPokemonName } from 'utils/helpers/convert-pokemon-name';

interface IPokemonCardProps {
  pokemon: IPokedexPokemon;
}

const PokemonCard: FC<IPokemonCardProps> = ({ pokemon }) => {
  const dispatch = useDispatch<AppDispatch>();
  const location = useLocation();

  const [isLoading, setIsLoading] = useState<boolean>(true);

  const isRandomPokedex =
    location.search.includes(pokedexText.random) ||
    location.search.includes(pokedexText.favorites);

  useEffect(() => {
    if (!isRandomPokedex) {
      dispatch(fetchPokemonInfo(pokemon.name));
    }
  }, []);

  const handleImageLoading = () => {
    setIsLoading(false);
  };

  const renderImage = (mainImage: string) => {
    if (mainImage) {
      return (
        <>
          <QuestionIcon className="question loading" />
          <img src={mainImage} onLoad={handleImageLoading} />
        </>
      );
    }

    return <QuestionIcon className="question" />;
  };

  const renderPokemonCard = () => {
    if (pokemon.pokemonInfo) {
      const {
        id,
        name,
        types,
        isFavorite,
        sprites: {
          homeFrontShiny,
          homeFrontDefault,
          spriteBackDefault,
          spriteFrontDefault,
          officialFrontDefault,
        },
      } = pokemon.pokemonInfo;

      const pokemonId = id.toString().padStart(padTargetLength, padString);

      const mainImage =
        officialFrontDefault || homeFrontDefault || homeFrontShiny || '';

      const handleLike = () => {
        dispatch(likePokemon({ pokemonId: id, isFavorite: !isFavorite }));
        addPokemonToFavorites(id, isFavorite);
      };

      return (
        <CardActionArea>
          <ImageContainer
            isLoading={isLoading}
            isQuestion={!mainImage}
            isFavorite={isFavorite}
          >
            {renderImage(mainImage)}
            <LikeButton
              className="like-button"
              isLiked={isFavorite}
              onClick={handleLike}
            />
          </ImageContainer>

          <StyledContent>
            <div>
              <div className="id">
                {commonText.number}
                {pokemonId}
              </div>

              <div className="pokemon-info">
                <div>
                  <div className="name">{convertPokemonName(name).name}</div>
                  <div className="version">
                    {convertPokemonName(name).version}
                  </div>
                </div>

                <PokemonTypes types={types} />
              </div>
            </div>

            <div className="sprites">
              <img src={spriteFrontDefault ?? ''} />
              <img src={spriteBackDefault ?? ''} />
            </div>
          </StyledContent>
        </CardActionArea>
      );
    }

    return <div className="no-data">{pokedexText.noData}</div>;
  };

  return <StyledPokemonCard>{renderPokemonCard()}</StyledPokemonCard>;
};

export default PokemonCard;

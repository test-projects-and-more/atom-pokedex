import { Card, CardContent } from '@mui/material';
import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const StyledPokemonCard = styled(Card)`
  display: flex;
  max-width: 18.75rem;
  flex-direction: column;

  &[class$='MuiCard-root'] {
    overflow: visible;
    border-radius: 16px;
    background-color: ${variables.color.pokedex.card.background};
  }

  button[class$='MuiCardActionArea-root'] {
    border-radius: 16px;
    padding: 1.5rem 0 1.5rem 1.5rem;
  }

  .MuiCardActionArea-focusHighlight {
    opacity: 0;
  }

  :hover {
    .sprites {
      opacity: 1;
      transform: translateX(-5%);
    }

    .like-button {
      opacity: 1;
      transform: translateX(0);
    }
  }

  .no-data {
    display: flex;
    height: 28rem;
    width: 18.75rem;
    font-weight: 600;
    font-size: 1.688rem;
    align-items: center;
    justify-content: center;
  }
`;

export const StyledContent = styled(CardContent)`
  display: flex;
  align-items: center;
  padding: 0 !important;
  justify-content: space-between;

  .pokemon-info {
    display: flex;
    min-height: 6.563rem;
    flex-direction: column;
    justify-content: space-between;
  }

  .id {
    font-weight: 600;
    margin-bottom: 1rem;
    margin-top: 0.625rem;
    color: ${variables.color.pokedex.card.secondaryText};
  }

  .name {
    font-weight: 600;
    font-size: 1.688rem;
    text-transform: capitalize;
  }

  .version {
    margin-bottom: 1rem;
    text-transform: capitalize;
    color: ${variables.color.pokedex.card.secondaryText};
  }

  .sprites {
    opacity: 0;
    display: flex;
    flex-direction: column;
    transform: translateX(30%);
    transition: all 0.5s ease-out;

    img {
      max-width: 4.375rem;

      :first-child {
        margin: 0.188rem 0;
      }
    }
  }
`;

interface IImageContainerProps {
  isLoading: boolean;
  isQuestion: boolean;
  isFavorite: boolean;
}

export const ImageContainer = styled.div<IImageContainerProps>`
  position: relative;
  background: ${variables.color.pokedex.card.img.container};

  display: ${({ isQuestion, isLoading }) =>
    isQuestion || isLoading ? 'flex' : ''};
  max-width: ${({ isQuestion, isLoading }) =>
    isQuestion || isLoading ? '17.25rem' : ''};
  min-width: ${({ isQuestion, isLoading }) =>
    isQuestion || isLoading ? '14.375rem' : ''};
  max-height: ${({ isQuestion, isLoading }) =>
    isQuestion || isLoading ? '15.938rem' : ''};
  min-height: ${({ isQuestion, isLoading }) =>
    isQuestion || isLoading ? '13.063rem' : ''};
  align-items: ${({ isQuestion, isLoading }) =>
    isQuestion || isLoading ? 'center' : ''};
  justify-content: ${({ isQuestion, isLoading }) =>
    isQuestion || isLoading ? 'center' : ''};

  img {
    display: ${({ isLoading }) => (isLoading ? 'none' : '')};
    max-height: 17.25rem;
    max-width: 18.75rem;
    width: 100%;
    height: 100%;
    margin-left: 2rem;
    margin-top: -1.563rem;
    filter: drop-shadow(
      -30px 10px 0px ${variables.color.pokedex.card.img.shadow}
    );
  }

  .question {
    width: 60%;
    height: 60%;
    margin: 16% 0;
    color: ${variables.color.pokedex.card.img.question};

    &.loading {
      display: ${({ isLoading }) => (isLoading ? '' : 'none')};
    }
  }

  .like-button {
    position: absolute;
    top: 0.313rem;
    left: 0.313rem;
    opacity: ${({ isFavorite }) => (isFavorite ? '1' : '0')};
    transform: ${({ isFavorite }) => (isFavorite ? '' : 'translateX(-30%)')};
    transition: all 0.5s ease-out;

    svg {
      width: 2.5rem;
      height: 2.5rem;
    }
  }
`;

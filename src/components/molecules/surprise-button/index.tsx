import { FC, useState } from 'react';

import AutoAwesomeOutlinedIcon from '@mui/icons-material/AutoAwesomeOutlined';
import Button from 'components/atoms/button';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import {
  changePokedexLoadingState,
  clearPokedex,
  fetchRandomPokemonInfo,
  updateRandomPokemonsLength,
} from 'redux/pokedex';
import { AppDispatch } from 'redux/store';
import { commonText, pokedexText } from 'utils/constants/dictionary';
import {
  pokemonTypesColors,
  surpriseButtonDefaultColor,
} from 'utils/constants/pokedex';
import { routes } from 'utils/constants/routes';
import { getRandomPokemons } from 'utils/helpers/get-random-pokemons';

interface ISurpriseButton {
  limit?: string;
}

const SurpriseButton: FC<ISurpriseButton> = ({ limit }) => {
  const dispatch = useDispatch<AppDispatch>();

  const navigate = useNavigate();

  const [color, setColor] = useState<number>(surpriseButtonDefaultColor);

  const getRandomButtonColor = () => {
    return Math.floor(Math.random() * pokemonTypesColors.length);
  };

  const handleSurpriseClick = () => {
    dispatch(clearPokedex());
    dispatch(changePokedexLoadingState(true));

    setColor(getRandomButtonColor());

    getRandomPokemons(Number(limit)).forEach((pokemonId) =>
      dispatch(fetchRandomPokemonInfo(pokemonId))
    );

    dispatch(updateRandomPokemonsLength(Number(limit)));

    navigate({
      pathname: routes.pokedex,
      search: `?${pokedexText.random}=${commonText.true}`,
    });
  };

  return (
    <Button
      onClick={handleSurpriseClick}
      startIcon={<AutoAwesomeOutlinedIcon />}
      buttonColor={pokemonTypesColors[color]}
    >
      {pokedexText.surpriseMe}
    </Button>
  );
};

export default SurpriseButton;

import { Button } from '@mui/material';
import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const PokemonType = styled(Button)<{ $pokemonType: string }>`
  &[class$='MuiButton-root'] {
    text-transform: capitalize;
    border-color: transparent !important;
    border-right: 1px solid transparent !important;
    color: ${variables.color.common.buttonText} !important;
    background-color: ${({ $pokemonType }) =>
      variables.color.pokedex.pokemonTypes[$pokemonType]} !important;
  }
`;

import { FC } from 'react';

import { ButtonGroup } from '@mui/material';
import { PokemonType } from 'components/molecules/pokemon-types/style';

interface IPokemonTypesProps {
  types: string[];
}

const PokemonTypes: FC<IPokemonTypesProps> = ({ types }) => {
  return (
    <ButtonGroup disableElevation variant="contained">
      {types.map((type) => {
        return (
          <PokemonType key={type} $pokemonType={type} disabled>
            {type}
          </PokemonType>
        );
      })}
    </ButtonGroup>
  );
};

export default PokemonTypes;

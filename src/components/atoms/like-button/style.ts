import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const StyledButton = styled.div<{ isLiked: boolean }>`
  z-index: 5;
  color: ${({ isLiked }) =>
    isLiked
      ? `${variables.color.common.likeButton.active}`
      : `${variables.color.common.likeButton.inactive}`};

  :hover {
    color: ${({ isLiked }) =>
      isLiked
        ? `${variables.color.common.likeButton.hoverActive}`
        : `${variables.color.common.likeButton.hoverInactive}`};
  }
`;

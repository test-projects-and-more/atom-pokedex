import { FC } from 'react';

import HeartIcon from 'assets/icons/heart-icon';
import { StyledButton } from 'components/atoms/like-button/style';

interface ILikeButtonProps {
  isLiked: boolean;
  className: string;
  onClick: () => void;
}

const LikeButton: FC<ILikeButtonProps> = ({ isLiked, className, onClick }) => {
  return (
    <StyledButton className={className} isLiked={isLiked} onClick={onClick}>
      <HeartIcon />
    </StyledButton>
  );
};

export default LikeButton;

import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const StyledLogo = styled(Link)`
  svg {
    filter: contrast(1.3) saturate(3);
  }
`;

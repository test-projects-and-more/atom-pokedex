import { FC } from 'react';

import LogoIcon from 'assets/icons/logo-icon';
import { StyledLogo } from 'components/atoms/logo/style';
import { routes } from 'utils/constants/routes';

const Logo: FC = () => {
  return (
    <StyledLogo to={routes.pokedex}>
      <LogoIcon />
    </StyledLogo>
  );
};

export default Logo;

import { FC } from 'react';

import { StyledSpinner } from 'components/atoms/spinner/style';

const Spinner: FC = () => {
  return <StyledSpinner />;
};

export default Spinner;

import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const StyledSpinner = styled.div`
  position: relative;
  width: 6.25rem;
  height: 6.25rem;
  border-radius: 50%;
  margin-bottom: 6rem;
  border: 4px solid transparent;
  animation: spin 1.5s linear infinite;
  border-top-color: ${variables.color.common.spinner.external};
  border-left-color: ${variables.color.common.spinner.external};

  ::before {
    position: absolute;
    top: 0.938rem;
    left: 0.938rem;
    right: 0.938rem;
    bottom: 0.938rem;
    content: '';
    border-radius: 50%;
    border: 4px solid transparent;
    animation: spinBack 1s linear infinite;
    border-top-color: ${variables.color.common.spinner.internal};
    border-left-color: ${variables.color.common.spinner.internal};
  }

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    50% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }

  @keyframes spinBack {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(-720deg);
    }
  }
`;

import { FC, ReactNode } from 'react';

import { ButtonProps } from '@mui/material';
import { StyledButton } from 'components/atoms/button/style';

interface IButtonProps extends ButtonProps {
  className?: string;
  children: ReactNode;
  onClick: () => void;
  buttonColor?: string;
  variant?: 'contained' | 'outlined' | 'text';
}

const Button: FC<IButtonProps> = ({
  variant,
  onClick,
  children,
  className,
  buttonColor,
  ...rest
}) => {
  return (
    <StyledButton
      onClick={onClick}
      className={className}
      $buttonColor={buttonColor}
      variant={variant ?? 'contained'}
      {...rest}
    >
      {children}
    </StyledButton>
  );
};

export default Button;

import { Button } from '@mui/material';
import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const StyledButton = styled(Button)<{ $buttonColor?: string }>`
  &[class$='MuiButton-root'] {
    color: ${variables.color.common.buttonText};
    box-shadow: none;
    min-width: 6.25rem;
    text-transform: capitalize;
    background-color: ${({ $buttonColor }) => $buttonColor ?? ''};

    :hover {
      background-color: ${({ $buttonColor }) => $buttonColor ?? ''};
    }
  }
`;

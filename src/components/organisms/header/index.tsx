import { FC } from 'react';

import Logo from 'components/atoms/logo';
import {
  Menu,
  StyledHeader,
  HeaderWrapper,
} from 'components/organisms/header/style';
import { Link, useLocation } from 'react-router-dom';
import { headerMenuItems } from 'utils/constants/header-menu-items';

const Header: FC = () => {
  const location = useLocation();

  const currentPath = location.pathname;

  return (
    <StyledHeader>
      <HeaderWrapper>
        <Logo />

        <Menu>
          {headerMenuItems.map((menuItem) => {
            return (
              <Link
                key={menuItem.name}
                to={menuItem.path}
                className={`${
                  currentPath === menuItem.path
                    ? 'active menu-item'
                    : 'menu-item'
                }`}
              >
                {menuItem.name}
              </Link>
            );
          })}
        </Menu>
      </HeaderWrapper>
    </StyledHeader>
  );
};

export default Header;

import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const StyledHeader = styled.header`
  height: ${variables.sizes.headerHeight};
  border-top: 1.25rem solid ${variables.color.common.background};
  background: linear-gradient(
    to bottom right,
    ${variables.color.header.green} 0%,
    ${variables.color.header.lightGreen} 35%,
    ${variables.color.header.yellow} 100%
  );
`;

export const HeaderWrapper = styled.div`
  display: flex;
  margin: 0 auto;
  padding: 0.2rem 2.5rem;
  justify-content: space-between;
  max-width: ${variables.sizes.wrapperContainerWidth};
`;

export const Menu = styled.div`
  width: 30%;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  .menu-item {
    opacity: 0.8;
    font-weight: 600;
    font-size: 1.25rem;
    text-decoration: none;
    text-transform: uppercase;
    color: ${variables.color.header.menuItem.inactive};

    &.active {
      color: ${variables.color.header.menuItem.active};
    }

    :hover {
      opacity: 1;
    }
  }
`;

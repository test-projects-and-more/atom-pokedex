import { FC, useEffect } from 'react';

import { SelectChangeEvent } from '@mui/material';
import Pagination from 'components/molecules/pagination';
import PokemonCard from 'components/molecules/pokemon-card';
import SpinnerContainer from 'components/molecules/spinner-container';
import {
  StyledContainer,
  PokedexContainer,
} from 'components/organisms/pokedex/style';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
  changePokedexLimit,
  changePokedexLoadingState,
  clearPokedex,
  fetchAllPokemons,
  fetchFilteredPokemons,
  fetchRandomPokemonInfo,
  updatePokedexPagesCount,
  updatePokedexPaginationOffset,
  updateRandomPokemonsLength,
} from 'redux/pokedex';
import {
  getPokemons,
  getPokedexLimit,
  getPokedexPagesCount,
  getPokedexLoadingState,
} from 'redux/pokedex/selectors';
import { AppDispatch } from 'redux/store';
import { commonText, pokedexText } from 'utils/constants/dictionary';
import { favoritesLocalStorage } from 'utils/constants/local-storage';
import { startPage } from 'utils/constants/pokedex';
import { routes } from 'utils/constants/routes';
import { getFromLocalStorage } from 'utils/helpers/get-from-local-storage';
import { getPaginationPagesCount } from 'utils/helpers/get-pagination-info';
import { getRandomPokemons } from 'utils/helpers/get-random-pokemons';
import useDidMountEffect from 'utils/hooks/use-did-mount-effect';

const Pokedex: FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const pokemons = useSelector(getPokemons);
  const pokedexLimit = useSelector(getPokedexLimit);
  const pokedexPagesCount = useSelector(getPokedexPagesCount);
  const isPokedexLoading = useSelector(getPokedexLoadingState);

  const location = useLocation();
  const navigate = useNavigate();

  const pokedexParams = new URLSearchParams(location.search);
  const pageNumber = pokedexParams.get(commonText.page);
  const typeFilter = pokedexParams.get(pokedexText.type);
  const regionFilter = pokedexParams.get(pokedexText.region);
  const randomFilter = pokedexParams.get(pokedexText.random);
  const favoritesFilter = pokedexParams.get(pokedexText.favorites);
  const pokedexPageNumber = pageNumber ? Number(pageNumber) : startPage;

  const isRandomPokedex = location.search.includes(pokedexText.random);

  const locationSearch =
    location.search.indexOf(commonText.ampersand) !== -1
      ? location.search.slice(location.search.indexOf(commonText.ampersand))
      : '';

  const handlePaginationChange = () => {
    dispatch(
      updatePokedexPaginationOffset({
        pageNumber: pokedexPageNumber,
        limit: pokedexLimit,
      })
    );
  };

  const handleLimitChange = (event: SelectChangeEvent) => {
    const limit = event.target.value;

    dispatch(changePokedexLimit(limit));
  };

  const handleGetPokedexData = () => {
    if (regionFilter || typeFilter) {
      dispatch(
        fetchFilteredPokemons({
          typeFilter: typeFilter ?? '',
          regionFilter: regionFilter ?? '',
        })
      );

      return;
    }

    dispatch(clearPokedex());

    if (randomFilter) {
      dispatch(changePokedexLoadingState(true));
      getRandomPokemons(Number(pokedexLimit)).forEach((pokemonId) =>
        dispatch(fetchRandomPokemonInfo(pokemonId))
      );

      dispatch(updateRandomPokemonsLength(Number(pokedexLimit)));

      return;
    }

    if (favoritesFilter) {
      const favorites = getFromLocalStorage(favoritesLocalStorage);

      dispatch(changePokedexLoadingState(true));

      dispatch(
        updatePokedexPagesCount(
          getPaginationPagesCount({
            count: favorites.length,
            limit: Number(pokedexLimit),
          })
        )
      );

      dispatch(updateRandomPokemonsLength(favorites.length));

      favorites.forEach((favoriteId: number) =>
        dispatch(fetchRandomPokemonInfo(favoriteId))
      );

      return;
    }

    dispatch(
      fetchAllPokemons({
        pageNumber: pokedexPageNumber,
        limit: Number(pokedexLimit),
      })
    );
  };

  useEffect(() => {
    handleGetPokedexData();

    if (!pageNumber && !isRandomPokedex) {
      navigate({
        pathname: routes.pokedex,
        search: `?${commonText.page}=${startPage}`,
      });
    }
  }, []);

  useDidMountEffect(() => {
    handleGetPokedexData();
  }, [pokedexLimit, location.search]);

  return (
    <>
      {isPokedexLoading && <SpinnerContainer />}

      <StyledContainer isLoading={isPokedexLoading}>
        <PokedexContainer>
          {Boolean(pokemons.length) &&
            pokemons.map((pokemon) => {
              return <PokemonCard key={pokemon.name} pokemon={pokemon} />;
            })}
        </PokedexContainer>

        {!isRandomPokedex && (
          <Pagination
            limit={pokedexLimit}
            path={routes.pokedex}
            params={locationSearch}
            pageNumber={pokedexPageNumber}
            pagesCount={pokedexPagesCount}
            defaultLimitValue={pokedexLimit}
            handleLimitChange={handleLimitChange}
            handlePaginationChange={handlePaginationChange}
          />
        )}
      </StyledContainer>
    </>
  );
};

export default Pokedex;

import styled from 'styled-components';

export const PokedexContainer = styled.div`
  display: grid;
  gap: 2rem;
  margin-bottom: 1rem;
  align-items: center;
  justify-content: center;
  justify-items: center;
  padding: 0 2.25rem;
  grid-template-columns: 1fr 1fr 1fr;
`;

export const StyledContainer = styled.div<{ isLoading: boolean }>`
  width: 100%;
  height: 100%;
  display: ${({ isLoading }) => (isLoading ? 'none' : 'block')};
`;

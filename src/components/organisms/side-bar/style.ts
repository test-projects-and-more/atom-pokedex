import { variables } from 'assets/styles/variables';
import styled from 'styled-components';

export const Menu = styled.div`
  position: sticky;
  top: 1.25rem;
  display: flex;
  height: 100%;
  flex-direction: column;
  width: ${variables.sizes.sidebarWidth};

  .title {
    font-weight: 600;
    font-size: 1.25rem;
    margin-bottom: 1.25rem;
    text-transform: uppercase;
  }

  .filters-button {
    margin: 1.25rem 0;
  }
`;

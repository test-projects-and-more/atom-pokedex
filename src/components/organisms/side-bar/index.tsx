import { FC } from 'react';

import RestartAltIcon from '@mui/icons-material/RestartAlt';
import SearchIcon from '@mui/icons-material/Search';
import { variables } from 'assets/styles/variables';
import Button from 'components/atoms/button';
import Accordion from 'components/molecules/accordion';
import FilterButtons from 'components/molecules/filter-buttons';
import RandomPanel from 'components/molecules/random-panel';
import { Menu } from 'components/organisms/side-bar/style';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import {
  updatePokedexRegionFilter,
  updatePokedexTypeFilter,
} from 'redux/pokedex';
import { getPokedexFilters } from 'redux/pokedex/selectors';
import { AppDispatch } from 'redux/store';
import { commonText, pokedexText } from 'utils/constants/dictionary';
import {
  regionFilterValues,
  startPage,
  typeFilterValues,
} from 'utils/constants/pokedex';
import { routes } from 'utils/constants/routes';

const SideBar: FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const pokedexFilters = useSelector(getPokedexFilters);

  const { typeFilter, regionFilter } = pokedexFilters;

  const navigate = useNavigate();

  const handleGetType = (type: string) => {
    if (typeFilter !== type) {
      dispatch(updatePokedexTypeFilter(type));
      return;
    }

    dispatch(updatePokedexTypeFilter(''));
  };

  const handleGetRegion = (region: string) => {
    if (regionFilter !== region) {
      dispatch(updatePokedexRegionFilter(region));
      return;
    }

    dispatch(updatePokedexRegionFilter(''));
  };

  const getPokedexFilterParams = () => {
    if (regionFilter && typeFilter) {
      return `${pokedexText.region}=${regionFilter}&${pokedexText.type}=${typeFilter}`;
    }

    if (regionFilter) {
      return `${pokedexText.region}=${regionFilter}`;
    }

    if (typeFilter) {
      return `${pokedexText.type}=${typeFilter}`;
    }
  };

  const handleSearch = () => {
    if (regionFilter || typeFilter) {
      navigate({
        pathname: routes.pokedex,
        search: `?${commonText.page}=${startPage}&${getPokedexFilterParams()}`,
      });
    }
  };

  const handleReset = () => {
    navigate({
      pathname: routes.pokedex,
      search: `?${commonText.page}=${startPage}`,
    });
  };

  return (
    <Menu>
      <div className="title">{pokedexText.filters}</div>

      <div>
        <Accordion
          title={pokedexText.regions}
          content={
            <FilterButtons
              filter={regionFilter}
              filterValues={regionFilterValues}
              handleButtonClick={handleGetRegion}
            />
          }
        />

        <Accordion
          title={pokedexText.types}
          content={
            <FilterButtons
              filter={typeFilter}
              filterValues={typeFilterValues}
              handleButtonClick={handleGetType}
            />
          }
        />
      </div>

      <div className="filters-button">
        <Button
          onClick={handleSearch}
          startIcon={<SearchIcon />}
          buttonColor={variables.color.sideMenu.button.search}
        >
          {commonText.search}
        </Button>
      </div>

      <RandomPanel />

      <div className="filters-button">
        <Button
          onClick={handleReset}
          startIcon={<RestartAltIcon />}
          buttonColor={variables.color.sideMenu.button.reset}
        >
          {pokedexText.reset}
        </Button>
      </div>
    </Menu>
  );
};

export default SideBar;

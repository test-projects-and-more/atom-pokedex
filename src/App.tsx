import MainPage from 'pages/main-page';
import {
  Route,
  Routes,
  Navigate,
  BrowserRouter as Router,
} from 'react-router-dom';
import { routes } from 'utils/constants/routes';

function App() {
  return (
    <Router>
      <Routes>
        <Route path={routes.pokedex} element={<MainPage />} />
        <Route path="*" element={<Navigate to={routes.pokedex} replace />} />
      </Routes>
    </Router>
  );
}

export default App;

import { FC } from 'react';

import Header from 'components/organisms/header';
import Pokedex from 'components/organisms/pokedex';
import SideBar from 'components/organisms/side-bar';
import { MainContainer } from 'pages/main-page/style';

const MainPage: FC = () => {
  return (
    <>
      <Header />
      <MainContainer className="main">
        <SideBar />
        <Pokedex />
      </MainContainer>
    </>
  );
};

export default MainPage;

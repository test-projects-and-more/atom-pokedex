import styled from 'styled-components';

export const MainContainer = styled.main`
  display: flex;
  padding: 1.25rem 0 0 2.25rem;
  justify-content: space-between;
`;

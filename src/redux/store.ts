import { combineReducers, configureStore } from '@reduxjs/toolkit';
import pokedexSlice from 'redux/pokedex/index';

const rootReducer = combineReducers({
  pokedex: pokedexSlice,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type AppDispatch = typeof store.dispatch;

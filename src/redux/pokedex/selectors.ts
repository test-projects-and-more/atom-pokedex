import { IStore } from 'models/store';

export const getPokemons = (state: IStore) => state.pokedex.pokemons;

export const getPokedexPagesCount = (state: IStore) => state.pokedex.pagesCount;

export const getPokedexFilters = (state: IStore) => state.pokedex.filters;

export const getPokedexLimit = (state: IStore) => state.pokedex.limit;

export const getPokedexLoadingState = (state: IStore) =>
  state.pokedex.isLoading;

export const getRandomPokemons = (state: IStore) =>
  state.pokedex.randomPokemons;

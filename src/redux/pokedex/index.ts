import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  getAllPokemonsPath,
  getPokemonInfoPath,
  getPokemonRegionInfoPath,
  getPokemonTypeInfoPath,
} from 'api/pokedex';
import axios from 'axios';
import { IRequestResponse } from 'models/common-response';
import { IFilteredPokemonsRequestResponse } from 'models/filtered-pokemons-response';
import { IPokedexStore, IPokedexPokemon } from 'models/pokedex';
import { IPokemonRequestResponse } from 'models/pokemon-response';
import { actionsText } from 'utils/constants/dictionary';
import {
  defaultPokedexFilters,
  limits,
  startPage,
} from 'utils/constants/pokedex';
import { getNormalizedPokemonInfo } from 'utils/helpers/get-normalized-pokemon-info';
import {
  getPaginationOffset,
  getPaginationPagesCount,
} from 'utils/helpers/get-pagination-info';

interface IFetchAllPokemonsProps {
  limit?: number;
  pageNumber?: number;
}

interface IFetchPokemonFilter {
  typeFilter?: string;
  regionFilter?: string;
}

export const fetchAllPokemons = createAsyncThunk(
  `${actionsText.pokedex.name}/${actionsText.pokedex.fetchAllPokemons}`,
  async (
    {
      pageNumber = startPage,
      limit = Number(limits[0]),
    }: IFetchAllPokemonsProps,
    { dispatch }
  ) => {
    dispatch(changePokedexLoadingState(true));
    dispatch(updatePokedexLoadingCount(0));
    dispatch(clearPokedex());

    const offset = getPaginationOffset({ pageNumber, limit });

    const response = await axios.get(
      `${getAllPokemonsPath}/?${actionsText.pokedex.limit}=${limit}&${actionsText.pokedex.offset}=${offset}`
    );

    dispatch(
      updatePokedexPagesCount(
        getPaginationPagesCount({ count: response.data.count, limit })
      )
    );

    return response.data;
  }
);

export const fetchPokemonInfo = createAsyncThunk(
  `${actionsText.pokedex.name}/${actionsText.pokedex.fetchPokemonInfo}`,
  async (pokemonName: string) => {
    const response = await axios.get(`${getPokemonInfoPath}/${pokemonName}`);

    return response.data;
  }
);

export const fetchRandomPokemonInfo = createAsyncThunk(
  `${actionsText.pokedex.name}/${actionsText.pokedex.fetchRandomPokemonInfo}`,
  async (pokemonId: number) => {
    const response = await axios.get(`${getPokemonInfoPath}/${pokemonId}`);

    return response.data;
  }
);

export const fetchPokemonType = createAsyncThunk(
  `${actionsText.pokedex.name}/${actionsText.pokedex.fetchPokemonType}`,
  async (type: string) => {
    const response = await axios.get(`${getPokemonTypeInfoPath}/${type}`);

    return response.data;
  }
);

export const fetchPokemonRegion = createAsyncThunk(
  `${actionsText.pokedex.name}/${actionsText.pokedex.fetchPokemonRegion}`,
  async (region: string) => {
    const regionResponse = await axios.get(
      `${getPokemonRegionInfoPath}/${region}`
    );

    const response = await axios.get(`${regionResponse.data.pokedexes[0].url}`);

    return response.data;
  }
);

export const fetchFilteredPokemons = createAsyncThunk(
  `${actionsText.pokedex.name}/${actionsText.pokedex.fetchFilteredPokemons}`,
  async ({ typeFilter, regionFilter }: IFetchPokemonFilter, { dispatch }) => {
    dispatch(clearPokedex());
    dispatch(changePokedexLoadingState(true));

    const typeResponse = typeFilter
      ? await dispatch(fetchPokemonType(typeFilter))
      : null;

    const regionResponse = regionFilter
      ? await dispatch(fetchPokemonRegion(regionFilter))
      : null;

    return {
      typeResponse: typeResponse?.payload,
      regionResponse: regionResponse?.payload,
    };
  }
);

const initialState: IPokedexStore = {
  pokemons: [],
  randomPokemons: [],
  offset: 0,
  pagesCount: 0,
  loadingCount: 0,
  isLoading: false,
  limit: limits[0],
  randomPokemonsLength: 0,
  filters: defaultPokedexFilters,
};

const pokedexSlice = createSlice({
  name: actionsText.pokedex.name,
  initialState,
  reducers: {
    clearPokedex(state: IPokedexStore) {
      state.pokemons = [];
      state.loadingCount = 0;
      state.randomPokemons = [];
      state.randomPokemonsLength = 0;
    },
    likePokemon(
      state: IPokedexStore,
      action: PayloadAction<{ isFavorite: boolean; pokemonId: number }>
    ) {
      const { isFavorite, pokemonId } = action.payload;

      state.pokemons = state.pokemons.map((pokemon) => {
        if (pokemon.pokemonInfo?.id === pokemonId) {
          return {
            ...pokemon,
            pokemonInfo: {
              ...pokemon.pokemonInfo,
              isFavorite,
            },
          };
        }

        return pokemon;
      });
    },
    updatePokedexPagesCount(
      state: IPokedexStore,
      action: PayloadAction<number>
    ) {
      state.pagesCount = action.payload;
    },
    changePokedexLimit(state: IPokedexStore, action: PayloadAction<string>) {
      state.limit = action.payload;
    },
    changePokedexLoadingState(
      state: IPokedexStore,
      action: PayloadAction<boolean>
    ) {
      state.isLoading = action.payload;
    },
    updatePokedexLoadingCount(
      state: IPokedexStore,
      action: PayloadAction<number>
    ) {
      state.loadingCount = action.payload;
    },
    updatePokedexRegionFilter(
      state: IPokedexStore,
      action: PayloadAction<string>
    ) {
      state.filters.regionFilter = action.payload;
    },
    updatePokedexTypeFilter(
      state: IPokedexStore,
      action: PayloadAction<string>
    ) {
      state.filters.typeFilter = action.payload;
    },
    updatePokedexPaginationOffset(
      state: IPokedexStore,
      action: PayloadAction<{
        pageNumber: number;
        limit: string;
      }>
    ) {
      const { pageNumber, limit } = action.payload;

      state.offset = getPaginationOffset({ pageNumber, limit: Number(limit) });
    },
    updateRandomPokemonsLength(
      state: IPokedexStore,
      action: PayloadAction<number>
    ) {
      state.randomPokemonsLength = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      fetchAllPokemons.fulfilled,
      (
        state: IPokedexStore,
        action: PayloadAction<IRequestResponse<IPokedexPokemon[]>>
      ) => {
        state.pokemons = action.payload.results;
      }
    );
    builder.addCase(
      fetchPokemonInfo.fulfilled,
      (
        state: IPokedexStore,
        action: PayloadAction<IPokemonRequestResponse>
      ) => {
        const { id, name, types, sprites } = action.payload;

        const {
          favorites,
          normalizedTypes,
          pokemonImages: {
            officialFrontDefault,
            spriteFrontDefault,
            spriteBackDefault,
            homeFrontDefault,
            homeFrontShiny,
          },
        } = getNormalizedPokemonInfo({ types, sprites });

        const pokemonPos = state.pokemons.findIndex(
          (pokemon) => pokemon.name === name
        );

        state.pokemons[pokemonPos].pokemonInfo = {
          id,
          name,
          types: normalizedTypes,
          sprites: {
            officialFrontDefault,
            spriteFrontDefault,
            spriteBackDefault,
            homeFrontDefault,
            homeFrontShiny,
          },
          isFavorite: favorites.includes(id),
        };

        state.loadingCount++;

        if (state.loadingCount === state.pokemons.length) {
          state.isLoading = false;
        }
      }
    );
    builder.addCase(fetchPokemonInfo.rejected, (state: IPokedexStore) => {
      state.loadingCount++;

      if (state.loadingCount === state.pokemons.length) {
        state.isLoading = false;
      }
    });
    builder.addCase(
      fetchRandomPokemonInfo.fulfilled,
      (
        state: IPokedexStore,
        action: PayloadAction<IPokemonRequestResponse>
      ) => {
        const { id, name, types, sprites } = action.payload;

        const {
          favorites,
          normalizedTypes,
          pokemonImages: {
            officialFrontDefault,
            spriteFrontDefault,
            spriteBackDefault,
            homeFrontDefault,
            homeFrontShiny,
          },
        } = getNormalizedPokemonInfo({ types, sprites });

        state.randomPokemons.push({
          name,
          pokemonInfo: {
            id,
            name,
            types: normalizedTypes,
            sprites: {
              officialFrontDefault,
              spriteFrontDefault,
              spriteBackDefault,
              homeFrontDefault,
              homeFrontShiny,
            },
            isFavorite: favorites.includes(id),
          },
        });

        state.loadingCount++;

        if (state.loadingCount === state.randomPokemonsLength) {
          state.pokemons = state.randomPokemons
            .sort((a, b) =>
              a.pokemonInfo?.id && b.pokemonInfo?.id
                ? a.pokemonInfo.id - b.pokemonInfo.id
                : 0
            )
            .slice(state.offset, state.offset + Number(state.limit));

          state.isLoading = false;
        }
      }
    );
    builder.addCase(fetchRandomPokemonInfo.rejected, (state: IPokedexStore) => {
      state.loadingCount++;

      if (state.loadingCount === state.pokemons.length) {
        state.isLoading = false;
      }
    });
    builder.addCase(
      fetchFilteredPokemons.fulfilled,
      (
        state: IPokedexStore,
        action: PayloadAction<IFilteredPokemonsRequestResponse>
      ) => {
        const { typeResponse, regionResponse } = action.payload;

        const filteredTypePokemons =
          typeResponse &&
          typeResponse.pokemon.map((pokemon) => pokemon.pokemon);

        const filteredRegionPokemons =
          regionResponse &&
          regionResponse.pokemon_entries.map(
            (pokemon) => pokemon.pokemon_species
          );

        if (typeResponse && regionResponse) {
          const filteredPokemons = filteredTypePokemons.filter(
            (typePokemon) => {
              return filteredRegionPokemons.find(
                (regionPokemon) => regionPokemon.name === typePokemon.name
              );
            }
          );

          state.pokemons = filteredPokemons.slice(
            state.offset,
            state.offset + Number(state.limit)
          );

          state.pagesCount = getPaginationPagesCount({
            count: filteredPokemons.length,
            limit: Number(state.limit),
          });

          return;
        }

        if (typeResponse) {
          state.pokemons = filteredTypePokemons.slice(
            state.offset,
            state.offset + Number(state.limit)
          );

          state.pagesCount = getPaginationPagesCount({
            count: filteredTypePokemons.length,
            limit: Number(state.limit),
          });

          return;
        }

        if (regionResponse) {
          state.pokemons = filteredRegionPokemons.slice(
            state.offset,
            state.offset + Number(state.limit)
          );

          state.pagesCount = getPaginationPagesCount({
            count: filteredRegionPokemons.length,
            limit: Number(state.limit),
          });
        }
      }
    );
  },
});

export const {
  likePokemon,
  clearPokedex,
  changePokedexLimit,
  updatePokedexTypeFilter,
  updatePokedexPagesCount,
  updatePokedexRegionFilter,
  updatePokedexLoadingCount,
  changePokedexLoadingState,
  updateRandomPokemonsLength,
  updatePokedexPaginationOffset,
} = pokedexSlice.actions;

export default pokedexSlice.reducer;

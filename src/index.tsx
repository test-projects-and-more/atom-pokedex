import App from 'App';
import GlobalStyle from 'assets/styles/global';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { store } from 'redux/store';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

//TODO Return StrictMode

root.render(
  <Provider store={store}>
    <GlobalStyle />
    <App />
  </Provider>
);

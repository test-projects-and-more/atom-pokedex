import { IVariables } from 'models/common';

export const variables: IVariables = {
  color: {
    common: {
      text: '#000000',
      buttonText: '#FFFFFF',
      background: '#FFFFFF',
      paginationBackground: '#FFFFFF',

      likeButton: {
        active: '#F34366',
        inactive: '#646464',
        hoverActive: '#F13056',
        hoverInactive: '#474747',
      },

      spinner: {
        external: '#9BCC50',
        internal: '#EED535',
      },
    },

    header: {
      green: '#82FF23',
      lightGreen: '#C2FF2F',
      yellow: '#FCFF37',

      menuItem: {
        active: '#B8860B',
        inactive: '#102EF2',
      },
    },

    sideMenu: {
      border: '#E9E9E9',

      button: {
        search: '#9BCC50',
        reset: '#9BCC50',
        favorites: '#F34366',
      },

      accordion: {
        backgroung: '#E9E9E9',
        border: 'rgba(0, 0, 0, 0.125)',
        collapseBackground: '#FFFFFF',
      },
    },

    pokedex: {
      pokemonTypes: {
        normal: '#A4ACAF',
        fighting: '#C02038',
        flying: '#3DC7EF',
        poison: '#B97FC9',
        ground: '#E0C068',
        rock: '#B8A038',
        bug: '#729F3F',
        ghost: '#7B62A3',
        steel: '#9EB7B8',
        fire: '#FD7D24',
        water: '#4592C4',
        grass: '#9BCC50',
        electric: '#EED535',
        ice: '#51C4E7',
        dragon: '#3C64C8',
        psychic: '#f366B9',
        dark: '#646464',
        fairy: '#FDB9E9',
      },

      regions: {
        kanto: '#80BB1D',
        johto: '#CAC02E',
        hoenn: '#67C1AB',
        sinnoh: '#9072A3',
        unova: '#6BAECE',
        kalos: '#CB0B4F',
        alola: '#DC5A40',
        galar: '#AC379E',
      },

      card: {
        background: '#E9E9E9',
        secondaryText: '#919191',

        img: {
          container: '#FFFFFF',
          question: '#E9E9E9',
          shadow: '#DFDFDF',
        },
      },
    },
  },

  sizes: {
    headerHeight: '106px',
    sidebarWidth: '30%',
    wrapperContainerWidth: '1440px',
  },
};

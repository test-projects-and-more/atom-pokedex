import { variables } from 'assets/styles/variables';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html {
    font-size: 1rem;
    overflow-y: auto;
    overflow-x: hidden;
    height: -webkit-fill-available;
    font-family: 'Roboto', arial, sans-serif;
  }
  
  body {
    margin: 0;
    height: 100vh;
    color: ${variables.color.common.text};
    background: ${variables.color.common.background};
  }

  #root {
    height: 100vh;
  }

  .calc-width {
    max-width: calc(100% - ${variables.sizes.sidebarWidth});
  }
  
  .main {
    margin: 0 auto;
    max-width: ${variables.sizes.wrapperContainerWidth};
  }

  &::-webkit-scrollbar {
    width: 6px;
    height: 6px;
  }

  &::-webkit-scrollbar-track {
    border-radius: 10px;
    background: rgba(0,0,0,0.1);
  }

  &::-webkit-scrollbar-thumb{
    border-radius: 10px;
    background: rgba(0,0,0,0.2);
  }

  &::-webkit-scrollbar-thumb:hover{
  	background: rgba(0,0,0,0.4);
  }
  
  &::-webkit-scrollbar-thumb:active{
  	background: rgba(0,0,0,.9);
  }
`;

export default GlobalStyle;

import { FC } from 'react';

import { ISvgProps } from 'models/common';

const HeartIcon: FC<ISvgProps> = ({
  width,
  height,
  className,
  ...rest
}: ISvgProps) => {
  return (
    <svg
      width={width}
      height={height}
      x="0"
      y="0"
      version="1.1"
      xmlSpace="preserve"
      className={className}
      viewBox="-15 0 580 580"
      xmlns="http://www.w3.org/2000/svg"
      enableBackground="new 0 0 544.582 544.582"
      {...rest}
    >
      <path
        fill="currentColor"
        d="M448.069 57.839c-72.675-23.562-150.781 15.759-175.721 87.898-24.938-72.215-103.045-111.46-175.72-87.898C23.111 81.784-16.975 160.885 6.894 234.708c22.95 70.38 235.773 258.876 263.006 258.876 27.234 0 244.801-188.267 267.751-258.876 23.944-73.976-16.142-153.077-89.582-176.869z"
      ></path>
    </svg>
  );
};

HeartIcon.defaultProps = {
  width: 544.582,
  height: 544.582,
};

export default HeartIcon;

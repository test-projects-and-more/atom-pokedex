import { IPokemonPropertyInfo, IPokemonType } from 'models/pokedex';

export interface IPokemonRequestResponse {
  id: number;
  name: string;
  order: number;
  height: number;
  weight: number;
  is_default: boolean;
  'base-experience': number;
  location_area_encounters: string;
  types: IPokemonType[];
  moves: IPokemonMove[];
  stats: IPokemonStat[];
  abilities: IPokemonAbility[];
  forms: IPokemonPropertyInfo[];
  held_items: IPokemonHeldItem[];
  past_types: IPokemonPastType[];
  species: IPokemonPropertyInfo[];
  sprites: IPokemonRequestSprites;
  game_indices: IPokemonGameIndice[];
}

interface IPokemonAbility {
  slot: number;
  is_hidden: boolean;
  ability: IPokemonPropertyInfo;
}

interface IPokemonStat {
  effort: number;
  base_stat: number;
  stat: IPokemonPropertyInfo;
}

interface IPokemonGameIndice {
  game_index: number;
  version: IPokemonPropertyInfo;
}

interface IPokemonHeldItem {
  item: IPokemonPropertyInfo;
  version_details: IPokemonVersionDetail[];
}

interface IPokemonVersionDetail {
  rarity: number;
  version: IPokemonPropertyInfo;
}

interface IPokemonMove {
  move: IPokemonPropertyInfo;
  version_group_details: IPokemonMoveVersionDetail[];
}

interface IPokemonMoveVersionDetail {
  level_learned_at: number;
  version_group: IPokemonPropertyInfo;
  move_learn_method: IPokemonPropertyInfo;
}

interface IPokemonPastType {
  types: IPokemonType[];
  genertion: IPokemonPropertyInfo;
}

export interface IPokemonRequestSprites {
  front_default: string | null;
  back_default: string | null;
  front_shiny: string | null;
  back_shiny: string | null;
  front_female: string | null;
  back_female: string | null;
  front_shiny_female: string | null;
  back_shiny_female: string | null;
  other: IPokemonOtherSprites;
  versions: IPokemonGeneration;
}

interface IPokemonOtherSprites {
  home: IPokemonHomeSprites;
  dream_world: IPokemonDreamWorldSprites;
  'official-artwork': IPokemonOfficialSprite;
}

interface IPokemonDreamWorldSprites {
  front_default: string | null;
  front_female: string | null;
}

interface IPokemonHomeSprites {
  front_default: string | null;
  front_female: string | null;
  front_shiny: string | null;
  front_shiny_female: string | null;
}

interface IPokemonOfficialSprite {
  front_default: string | null;
}

interface IPokemonGeneration {
  [key: string]: IPokemonGenerationSprites;
}

interface IPokemonGenerationSprites {
  [key: string]: IPokemonGenerationSpritesType;
}

interface IPokemonGenerationSpritesType {
  [key: string]: Record<string, string>;
}

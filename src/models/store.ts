import { IPokedexStore } from 'models/pokedex';

export interface IStore {
  pokedex: IPokedexStore;
}

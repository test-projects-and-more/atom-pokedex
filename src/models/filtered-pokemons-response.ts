import { IPokedexPokemon, IPokemonPropertyInfo } from 'models/pokedex';

export interface IFilteredPokemonsRequestResponse {
  typeResponse: IFilteredByTypeResponse;
  regionResponse: IFilteredByRegionResponse;
}

interface IFilteredByTypeResponse {
  id: number;
  name: string;
  names: ILanguageName[];
  game_indices: IGameIndice[];
  moves: IPokemonPropertyInfo[];
  generation: IPokemonPropertyInfo;
  damage_relations: ITypeDamageRelations;
  move_damage_class: IPokemonPropertyInfo;
  past_damage_relations: IPastDamageRelation[];
  pokemon: IFilteredByTypePokemon[];
}

interface ITypeDamageRelations {
  double_damage_from: IPokemonPropertyInfo[];
  double_damage_to: IPokemonPropertyInfo[];
  half_damage_from: IPokemonPropertyInfo[];
  half_damage_to: IPokemonPropertyInfo[];
  no_damage_from: IPokemonPropertyInfo[];
  no_damage_to: IPokemonPropertyInfo[];
}

interface IGameIndice {
  game_index: number;
  generation: IPokemonPropertyInfo;
}

interface ILanguageName {
  name: string;
  language: IPokemonPropertyInfo;
}

interface IPastDamageRelation {
  damage_relations: ITypeDamageRelations;
  generation: IPokemonPropertyInfo;
}

interface IFilteredByTypePokemon {
  slot: number;
  pokemon: IPokedexPokemon;
}

interface IFilteredByRegionResponse {
  id: number;
  name: string;
  names: ILanguageName[];
  is_main_series: boolean;
  region: IPokemonPropertyInfo;
  pokemon_entries: IPokemonEntry[];
  version_groups: IPokemonPropertyInfo[];
  descriptions: IRegionLanguageDescription[];
}

interface IRegionLanguageDescription {
  description: string;
  language: IPokemonPropertyInfo;
}

interface IPokemonEntry {
  entry_number: number;
  pokemon_species: IPokedexPokemon;
}

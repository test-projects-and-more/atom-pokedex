export interface ISvgProps {
  width?: number;
  height?: number;
  className?: string;
}

export interface IHeaderMenuItem {
  path: string;
  name: string;
}

export interface IVariables {
  color: {
    common: {
      text: string;
      buttonText: string;
      background: string;
      paginationBackground: string;

      likeButton: {
        active: string;
        inactive: string;
        hoverActive: string;
        hoverInactive: string;
      };

      spinner: {
        external: string;
        internal: string;
      };
    };

    header: {
      green: string;
      lightGreen: string;
      yellow: string;

      menuItem: {
        active: string;
        inactive: string;
      };
    };

    sideMenu: {
      border: string;

      button: {
        search: string;
        reset: string;
        favorites: string;
      };

      accordion: {
        backgroung: string;
        border: string;
        collapseBackground: string;
      };
    };

    pokedex: {
      pokemonTypes: Record<string, string>;
      regions: Record<string, string>;

      card: {
        background: string;
        secondaryText: string;

        img: {
          container: string;
          question: string;
          shadow: string;
        };
      };
    };
  };

  sizes: {
    headerHeight: string;
    sidebarWidth: string;
    wrapperContainerWidth: string;
  };
}

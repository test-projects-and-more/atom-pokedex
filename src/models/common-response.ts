export interface IRequestResponse<T> {
  results: T;
  count: number;
  next: string | null;
  previos: string | null;
}

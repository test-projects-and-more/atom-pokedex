export interface IPokedexStore {
  pokemons: IPokedexPokemon[];
  randomPokemons: IPokedexPokemon[];
  randomPokemonsLength: number;
  filters: IPokedexFilters;
  limit: string;
  offset: number;
  pagesCount: number;
  isLoading: boolean;
  loadingCount: number;
}

export interface IPokedexPokemon {
  name: string;
  url?: string;
  pokemonInfo?: IPokedexPokemonCardInfo;
}

export interface IPokedexPokemonCardInfo {
  id: number;
  name: string;
  types: string[];
  isFavorite: boolean;
  sprites: IPokemonCardSprites;
}

export interface IPokemonCardSprites {
  officialFrontDefault: string | null;
  spriteFrontDefault: string | null;
  spriteBackDefault: string | null;
  homeFrontDefault: string | null;
  homeFrontShiny: string | null;
}

export interface IPokemonInfo {
  id: number;
  name: string;
  height: number;
  weight: number;
  types: string[];
  isFavorite: boolean;
  sprites: IPokemonSprites;
  forms: IPokemonPropertyInfo[];
}

export interface IPokemonSprites {
  spriteFrontDefault: string | null;
  spriteBackDefault: string | null;
  spriteFrontDefaultFemale: string | null;
  spriteBackDefaultFemale: string | null;
  spriteFrontShiny: string | null;
  spriteBackShiny: string | null;
  spriteFrontShinyFemale: string | null;
  spriteBackShinyFemale: string | null;
  animatedFrontDefault: string | null;
  animatedFrontShiny: string | null;
  officialFrontDefault: string | null;
  homeFrontDefault: string | null;
  homeFrontDefaultFemale: string | null;
  homeFrontShiny: string | null;
  homeFrontShinyFemale: string | null;
  icon: string | null | Record<string, string>;
}

export interface IPokemonType {
  slot: number;
  type: IPokemonPropertyInfo;
}

export interface IPokemonPropertyInfo {
  name: string;
  url: string;
}

export interface IPokedexFilters {
  typeFilter: string;
  regionFilter: string;
}

const base = 'https://pokeapi.co/api/v2/';
const pokemon = 'pokemon';
const type = 'type';
const region = 'region';
const pokedex = 'pokedex';

export const getAllPokemonsPath = `${base}${pokemon}`;
export const getPokemonInfoPath = `${base}${pokemon}`;
export const getPokemonTypeInfoPath = `${base}${type}`;
export const getPokemonRegionInfoPath = `${base}${region}`;
export const getRegionPokemonsInfoPath = `${base}${pokedex}`;
